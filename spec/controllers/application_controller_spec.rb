require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  describe "GET /health" do
    let(:response) { get(:health) }

    it "should return OK with 200 status" do
      expect(response).to have_http_status(:success)
    end

    it 'should return OK body' do
      expect(response.body).to match("OK")
    end

    it 'should return content type' do
      expect(response.content_type).to eq("application/json; charset=utf-8")
    end
  end
end
