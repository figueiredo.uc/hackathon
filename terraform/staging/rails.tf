resource "kubernetes_deployment" "rails" {
  metadata {
    name = local.rails_service_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  spec {
    selector {
      match_labels = {
        workloadselector = local.rails_workloadselector
      }
    }

    template {
      metadata {
        labels = {
          workloadselector = local.rails_workloadselector
        }
      }

      spec {
        container {
          name = local.rails_service_name

          env_from {
            secret_ref {
              name = kubernetes_secret.rails.metadata[0].name
            }
          }

          image = local.rails_container_image

          readiness_probe {
            http_get {
              http_header {
                name = "Host"
                value = kubernetes_secret.rails.data["HOST"]
              }

              path = "/health"
              port = 3000
            }
          }

          resources {
            limits = {
              memory = "1Gi"
            }
            
            requests = {
              memory = "0.5Gi"
            }
          }
        }

        image_pull_secrets {
          name = local.container_registry_secret_name
        }
      }
    }
  }
}

resource "kubernetes_service" "rails" {
  metadata {
    name = local.rails_service_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  spec {
    cluster_ip = "None"

    port {
      port = 3000
    }

    selector = {
      workloadselector = local.rails_workloadselector
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_ingress" "rails" {
  metadata {
    annotations = {
      "cert-manager.io/cluster-issuer" = "letsencrypt"
    }

    name = local.rails_service_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  spec {
    rule {
      host = var.secrets.host

      http {
        path {
          path = "/"

          backend {
            service_name = local.rails_service_name
            service_port = 3000
          } 
        }
      }
    }

    tls {
      hosts = [var.secrets.host]
      secret_name = "rails-boilerplate-stg-theloop-tech-cert"
    }
  }
}