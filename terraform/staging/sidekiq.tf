# Uncomment if you need sidekiq
# resource "kubernetes_deployment" "sidekiq" {
#   metadata {
#     name = local.sidekiq_service_name
#     namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
#   }

#   spec {
#     selector {
#       match_labels = {
#         workloadselector = local.sidekiq_workloadselector
#       }
#     }

#     template {
#       metadata {
#         labels = {
#           workloadselector = local.sidekiq_workloadselector
#         }
#       }

#       spec {
#         container {
#           name = local.sidekiq_service_name
#           image = local.rails_container_image
#           command = ["docker/production/sidekiq-entrypoint"]

#           env_from {
#             secret_ref {
#               name = kubernetes_secret.rails.metadata[0].name
#             }
#           }

#           resources {
#             limits = {
#               memory = "0.5Gi"
#             }
            
#             requests = {
#               memory = "0.25Gi"
#             }
#           }
#         }

#         image_pull_secrets {
#           name = local.container_registry_secret_name
#         }
#       }
#     }
#   }
# }

# resource "kubernetes_service" "sidekiq" {
#   metadata {
#     name = local.sidekiq_service_name
#     namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
#   }

#   spec {
#     cluster_ip = "None"

#     selector = {
#       workloadselector = local.sidekiq_workloadselector
#     }

#     type = "ClusterIP"
#   }
# }