terraform {
  backend "http" {
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "loop-staging-new"
}