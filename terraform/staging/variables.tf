variable "registry" {
  type = object({
    username = string
    password = string
  })

  sensitive = true
}

variable "namespace" {
  type = string
  default = "boilerplate-rails-staging"
}

variable "secrets" {
  type = object({
    database = object({
      password = string
      username = string
    })

    host = string

    # Uncomment if you need email delivery
    # mailer = object({
    #   host = string
    #   password = string
    #   port = number
    #   username = string
    # })

    sentry = object({
      dsn = string
    })
  })

  sensitive = true
}