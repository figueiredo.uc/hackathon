# Uncomment if you need redis cache store or sidekiq
# resource "kubernetes_deployment" "redis" {
#   metadata {
#     name = local.redis_service_name
#     namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
#   }

#   spec {
#     selector {
#       match_labels = {
#         workloadselector = local.redis_workloadselector
#       }
#     }

#     template {
#       metadata {
#         labels = {
#           workloadselector = local.redis_workloadselector
#         }
#       }

#       spec {
#         container {
#           name = local.redis_service_name
#           image = "redis:6"

#           resources {
#             limits = {
#               memory = "0.25Gi"
#             }
            
#             requests = {
#               memory = "0.15Gi"
#             }
#           }
#         }
#       }
#     }
#   }
# }

# resource "kubernetes_service" "redis" {
#   metadata {
#     name = local.redis_service_name
#     namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
#   }

#   spec {
#     cluster_ip = "None"

#     port {
#       port = 6379
#     }

#     selector = {
#       workloadselector = local.redis_workloadselector
#     }

#     type = "ClusterIP"
#   }
# }