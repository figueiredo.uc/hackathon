# LOCALS #
locals {
  container_registry_secret_name = "gitlab-boilerplate-rails"
  rails_container_image = "registry.gitlab.com/theloopco/boilerplates/rails"
}

locals {
  postgres_service_name = "postgres"
  postgres_workloadselector = "${kubernetes_namespace.boilerplate-rails-staging.metadata[0].name}-${local.postgres_service_name}"
}

locals {
  rails_service_name = "rails"
  rails_workloadselector = "${kubernetes_namespace.boilerplate-rails-staging.metadata[0].name}-${local.rails_service_name}"
}

# Uncomment if you need redis cache store or sidekiq
# locals {
#   redis_service_name = "redis"
#   redis_workloadselector = "${kubernetes_namespace.boilerplate-rails-staging.metadata[0].name}-${local.redis_service_name}"
# }

# Uncomment if you need sidekiq
# locals {
#   sidekiq_service_name = "sidekiq"
#   sidekiq_workloadselector = "${kubernetes_namespace.boilerplate-rails-staging.metadata[0].name}-${local.sidekiq_service_name}"
# }

locals {
  database_init_job_name = "database-init"
}

# NAMESPACES #
resource "kubernetes_namespace" "boilerplate-rails-staging" {
  metadata {
    name = var.namespace

    annotations = {
      "field.cattle.io/projectId"  = "c-t6lx8:p-t8jg6"
    }

    labels = {
      "field.cattle.io/projectId" = "p-t8jg6"
    }
  }
}

# REGISTRIES #
resource "kubernetes_secret" "gitlab-boilerplate-rails" {
  metadata {
    name = local.container_registry_secret_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "registry.gitlab.com": {
      "auth": "${base64encode("${var.registry.username}:${var.registry.password}")}"
    }
  }
}
DOCKER
  }
}

# SECRETS #
resource "kubernetes_secret" "rails" {
  metadata {
    name = local.rails_service_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  data = {
    DATABASE_USERNAME = var.secrets.database.username
    DATABASE_PASSWORD = var.secrets.database.password
    SENTRY_DSN = var.secrets.sentry.dsn
    # Uncomment if you need email delivery
    # MAILER_USERNAME = var.secrets.mailer.username
    # MAILER_HOST = var.secrets.mailer.host
    # MAILER_PORT = var.secrets.mailer.port
    # MAILER_PASSWORD = var.secrets.mailer.password
    HOST = var.secrets.host
  }
}