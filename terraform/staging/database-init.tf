resource "kubernetes_job" "database-init" {
  metadata {
    name = local.database_init_job_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  spec {
    template {
      metadata {}

      spec {
        container {
          name = local.database_init_job_name
          image = local.rails_container_image
          command = ["database-init"]

          env_from {
            secret_ref {
              name = kubernetes_secret.rails.metadata[0].name
            }
          }
        }

        image_pull_secrets {
          name = local.container_registry_secret_name
        }
      }
    }

    completions = 1
    backoff_limit = 6
  }

  wait_for_completion = true
}