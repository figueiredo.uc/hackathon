resource "kubernetes_persistent_volume_claim" "postgres" {
  metadata {
    name = local.postgres_service_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  spec {
    storage_class_name = "do-block-storage"
    access_modes = [ "ReadWriteOnce" ]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}

resource "kubernetes_deployment" "postgres" {
  metadata {
    name = local.postgres_service_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  spec {
    selector {
      match_labels = {
        workloadselector = local.postgres_workloadselector
      }
    }

    template {
      metadata {
        labels = {
          workloadselector = local.postgres_workloadselector
        }
      }

      spec {
        container {
          name = local.postgres_service_name
          image = "postgres:13"

          env {
            name = "POSTGRES_PASSWORD"

            value_from {
              secret_key_ref {
                key = "DATABASE_PASSWORD"
                name = kubernetes_secret.rails.metadata[0].name
              }
            }
          }

          env {
            name = "POSTGRES_USER"

            value_from {
              secret_key_ref {
                key = "DATABASE_USERNAME"
                name = kubernetes_secret.rails.metadata[0].name
              }
            }
          }

          resources {
            limits = {
              memory = "0.5Gi"
            }
            
            requests = {
              memory = "0.25Gi"
            }
          }

          volume_mount {
            mount_path = "/var/lib/postgresql/data"
            name = "general"
            sub_path = "data"
          }
        }

        volume {
          name = "general"
          persistent_volume_claim {
            claim_name = local.postgres_service_name
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "postgres" {
  metadata {
    name = local.postgres_service_name
    namespace = kubernetes_namespace.boilerplate-rails-staging.metadata[0].name
  }

  spec {
    cluster_ip = "None"

    port {
      port = 5432
    }

    selector = {
      workloadselector = local.postgres_workloadselector
    }

    type = "ClusterIP"
  }
}