**Clickup Card** https://app.clickup.com/t/:task_id

## Missing
*Temporary notes with missing features. Should be removed when MR is complete.*

## Summary
### Why
*Cause of the bug and general solution*

### Added

### Changed

### Removed

## Warnings
*Relevant notes, like not required missing features.*