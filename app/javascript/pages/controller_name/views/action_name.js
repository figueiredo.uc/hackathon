import ExamplePartial from './partials/example_partial'

document.addEventListener('turbolinks:load', () => {
  if (document.body.id == 'controller-name-action-name') {
    new ExamplePartial().setup()
  }
})