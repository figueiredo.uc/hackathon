#!/bin/sh
set -e

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker pull $DOCKER_CACHE_IMAGE

docker build --cache-from $DOCKER_CACHE_IMAGE -t $DOCKER_IMAGE \
  --build-arg SECRET_KEY_BASE=$SECRET_KEY_BASE \
  --build-arg DOCKER_CACHE_IMAGE=$DOCKER_CACHE_IMAGE \
  -f $DOCKERFILE_PATH .

docker push $DOCKER_IMAGE