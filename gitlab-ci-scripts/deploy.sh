#!/bin/bash
set -e

# SETUP #
kubectl config use-context $KUBECTL_CONTEXT

# RAILS #
kubectl --namespace=$KUBECTL_NAMESPACE set image deployment.apps/rails rails=$DOCKER_IMAGE