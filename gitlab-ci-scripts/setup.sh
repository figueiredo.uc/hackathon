#!/bin/bash
DATE=$(git show -s --date=format:%Y_%m_%d --format=%cd)

if [[ $CI_COMMIT_REF_NAME == master ]]; then
  KUBECTL_NAMESPACE=$PRD_KUBECTL_NAMESPACE
  KUBECTL_CONTEXT=$PRD_KUBECTL_CONTEXT
elif [[ $CI_COMMIT_REF_NAME == staging ]]; then
  KUBECTL_NAMESPACE=$STG_KUBECTL_NAMESPACE
  KUBECTL_CONTEXT=$STG_KUBECTL_CONTEXT
fi

echo "DOCKERFILE_PATH=docker/production/Dockerfile" >> .env
echo "DOCKER_IMAGE=${CI_REGISTRY_IMAGE}:${DATE}_${CI_COMMIT_SHORT_SHA}" >> .env
echo "DOCKER_CACHE_IMAGE=${CI_REGISTRY_IMAGE}:latest" >> .env
echo "KUBECTL_NAMESPACE=${KUBECTL_NAMESPACE}" >> .env
echo "KUBECTL_CONTEXT=${KUBECTL_CONTEXT}" >> .env

echo "=== Environment Variables ==="
cat .env